package main

import (
	"fmt"
	"regexp"
	"sort"
	"strings"
)

func main() {
	text := `cat and dog one dog two cats and one man`
	fmt.Println(TopWords(text))
}

func sortMap(list map[string]int) []string {
	wordList := make([]string, 0, len(list))
	for word := range list {
		wordList = append(wordList, word)
	}
	sort.Slice(wordList, func(i, j int) bool {
		return list[wordList[i]] > list[wordList[j]]
	})
	return wordList
}

func TopWords(text string) (result []string) {
	maxTop := 10
	words := make(map[string]int)
	regex := regexp.MustCompile(`([\wА-Яа-я]+)`)
	for _, word := range regex.FindAllString(text, -1) {
		word = strings.ToLower(word)
		words[word]++
	}
	result = sortMap(words)
	if lenWords := len(result); lenWords < maxTop {
		maxTop = lenWords
	}

	return result[:maxTop]
}
