package main

import (
	"testing"
)

const exampleTest = `one dog cat dog one cat cat dog dog`

func TestTop(t *testing.T) {
	top := TopWords(exampleTest)
	ok := []string{`dog`, `cat`, `one`}

	if len(top) != len(ok) {
		t.Error("Not corrects count")
		return
	}
	for i, v := range top {
		if v != ok[i] {
			t.Error("Not equal")
			return
		}
	}
}
